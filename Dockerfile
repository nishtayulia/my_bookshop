# встановлення базового образу - https://docs.docker.com/engine/reference/builder/#from
# https://medium.com/swlh/alpine-slim-stretch-buster-jessie-bullseye-bookworm-what-are-the-differences-in-docker-62171ed4531d
FROM python:3.11.2-alpine
# додаємо макроінформацію - хто автор і до кого задавать питання
# - https://docs.docker.com/engine/reference/builder/#maintainer-deprecated
LABEL maintainer="Yulia Panaseiko <Yulia.nishtayulia@gmail.com>"

ENV PYTHONDONTWRITEBYTECODE=1 PYTHONUNBUFFERED=1

COPY ./requirements.txt /requirements.txt
RUN pip install --upgrade pip
RUN  pip install -r requirements.txt

# Инициализация проекта
RUN mkdir /app
WORKDIR /app

COPY ./app /app
RUN adduser -D user

USER user